export const english = {
	language: "en",
	navbar: {
		login: "Login",
		account: "Account",
		cart: "Cart",
	},
	menubar: {
		shopall: "Shop All",
		clothes: "Clothes",
		shoes: "Shoes",
		bags: "Bags",
		accessories: "Accessories",
	},
}

export type LocaleType = typeof english
