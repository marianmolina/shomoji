export const french = {
	language: "fr",
	navbar: {
		login: "Se connecter",
		account: "Compte",
		cart: "Panier",
	},
	menubar: {
		shopall: "Tout",
		clothes: "Vêtements",
		shoes: "Chaussures",
		bags: "Sacs",
		accessories: "Accessoires",
	},
}
