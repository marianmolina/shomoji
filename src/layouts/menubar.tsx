import { useEffect, useState } from "react"

import { CartButtonLight } from "@/features/cart/cart-button-light"
import { useLocaleStore } from "@/stores"

export function MenuBar({
	shopRef,
	menuCategory,
	setMenuCategory,
	setFilter,
}: {
	shopRef: React.RefObject<HTMLDivElement>
	menuCategory: string
	setMenuCategory: React.Dispatch<React.SetStateAction<string>>
	setFilter: React.Dispatch<React.SetStateAction<boolean>>
}) {
	const translation = useLocaleStore((state) => state.translation)
	const menuCategories = [
		{ key: "shopall", value: translation.menubar.shopall },
		{ key: "clothes", value: translation.menubar.clothes },
		{ key: "shoes", value: translation.menubar.shoes },
		{ key: "bags", value: translation.menubar.bags },
		{ key: "accessories", value: translation.menubar.accessories },
	]
	const [showCartButton, setShowCartButton] = useState(false)

	useEffect(() => {
		const navigation = document.querySelector("#navigation") as Element
		const observer = new IntersectionObserver(
			(entry) => {
				if (entry[0].isIntersecting) setShowCartButton(false)
				else setShowCartButton(true)
			},
			{
				rootMargin: "100px",
				threshold: 1.0,
			},
		)
		observer.observe(navigation)
		return () => {
			observer.unobserve(navigation)
			observer.disconnect()
		}
	}, [])

	return (
		<div className="sticky top-0 z-10 flex justify-between  bg-white px-10 text-sm">
			<div>
				<ul className="flex">
					{menuCategories.map((item, index) => (
						<li key={index}>
							<button
								onClick={() => {
									setMenuCategory(() => item.key)
									setFilter(true)
									shopRef.current ? shopRef.current.scrollIntoView() : null
								}}
								className={`${
									item.key === menuCategory
										? " bg-slate-200 hover:cursor-default"
										: "hover:bg-slate-100"
								}  px-5 py-4 `}
							>
								{item.value.toUpperCase()}
							</button>
						</li>
					))}
				</ul>
			</div>
			<div>{showCartButton ? <CartButtonLight /> : <div />}</div>
		</div>
	)
}
