import { NavLink } from "react-router-dom"

import { CartButtonDark } from "@/features/cart/cart-button-dark"
import { Locale as LocaleType, useAuthStore, useLocaleStore } from "@/stores"

export function Header() {
	const translation = useLocaleStore((state) => state.translation)
	const setLocale = useLocaleStore((state) => state.setLocale)

	const isAuth = useAuthStore((state) => state.auth)

	return (
		<header>
			<div className="flex items-center justify-end gap-4 bg-slate-900 px-10 py-2 text-xs text-white">
				<select className="bg-slate-900" name="currency" id="currency">
					<option value="euro">Euro €</option>
				</select>
				<select
					className="bg-slate-900"
					name="locales"
					id="locales"
					value={translation.language}
					onChange={(e) => setLocale(e.target.value as LocaleType)}
				>
					<option value="en">English</option>
					<option value="fr">Français</option>
				</select>
			</div>
			<nav
				id="navigation"
				aria-label="primary navigation"
				className="flex items-center justify-between border-b bg-white px-10 py-4"
			>
				<NavLink to="/" className="text-3xl font-bold">
					Shomoji
				</NavLink>
				<div className="flex gap-4">
					{isAuth ? (
						<NavLink
							to="/account"
							className="rounded-full border border-slate-900 stroke-[#1D1D1D] px-5 py-2"
						>
							{translation.navbar.account}
						</NavLink>
					) : (
						<NavLink
							to="/login"
							className="rounded-full border border-slate-900 stroke-[#1D1D1D] px-5 py-2"
						>
							{translation.navbar.login}
						</NavLink>
					)}
					<CartButtonDark />
				</div>
			</nav>
		</header>
	)
}
