import { useRouteError } from "react-router-dom"

export function ErrorPage() {
	const error = useRouteError()

	return (
		<div className="m-10">
			<h1>Oops!</h1>
			<p>Sorry, an unexpected error has occured.</p>
		</div>
	)
}
