export function Footer() {
	return (
		<div className="border-t bg-slate-100 px-10 pt-10 pb-4 text-sm text-gray-800">
			<div className="flex justify-around">
				<div className="mr-4 max-w-sm md:max-w-lg ">
					<div className="mb-4 font-semibold uppercase">About this website</div>
					<div className="leading-7">
						A full stack type-safe and light-weight ecommerce prototype using Typescript, React,
						Zustand (global client state management), tRPC (type-safe API calls with React Query
						bindings on the client) and Prisma (ORM for Typescript and Node.js).
					</div>
					<div className="mt-4 flex gap-4">
						Credit:
						<a
							href="https://www.figma.com/community/file/1182976024086113944"
							className="underline"
						>
							Emojis
						</a>
						<a
							href="https://pixabay.com/users/aalmeidah-4277022/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=4741364"
							className="underline"
						>
							Countryside image
						</a>
					</div>
					<div></div>
				</div>
				<div className="">
					<div className="mb-4 font-semibold uppercase">Contact me</div>

					<div className="flex flex-col gap-2">
						<a
							href="mailto:hi.marianmolina@gmail.com"
							target="_blank"
							rel="noreferrer"
							className="flex items-center gap-2"
						>
							Email
						</a>
						<a
							href="https://marianmolina.com"
							target="_blank"
							rel="noreferrer"
							className="flex items-center gap-2"
						>
							Website
						</a>
						<a
							href="https://gitlab.com/marianmolina"
							target="_blank"
							rel="noreferrer"
							className="flex items-center gap-2"
						>
							Gitlab
						</a>
					</div>
				</div>
			</div>
			<div className="mt-14 text-center text-slate-600 lg:mb-0">©2023 Marian Molina</div>
		</div>
	)
}
