import { createTRPCReact } from "@trpc/react-query"

import type { Router } from "../../api"

export const trpc = createTRPCReact<Router>()
