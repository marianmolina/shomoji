import { CartProduct } from "../types"

export function getTotal(cart: CartProduct[]) {
	const total =
		cart?.length > 0
			? Math.ceil(
					cart
						.map((p: CartProduct) => p.totalPrice)
						.reduce((total: number, currentValue: number) => total + currentValue) * 100,
			  ) / 100
			: "0.00"

	const totalFormatted =
		total === "0.00"
			? total
			: total.toString().includes(".") &&
			  total.toString().split("").indexOf(".") >= total.toString().split("").length - 2
			? total.toString() + "0"
			: total.toString().length < 5
			? total.toString() + ".00"
			: total

	const nbItems =
		cart?.length > 0
			? cart

					.map((p: CartProduct) => p.amount)
					.reduce((total: number, currentValue: number) => total + currentValue)
			: 0

	return { totalFormatted, nbItems }
}
