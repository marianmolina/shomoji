import { Product } from "@prisma/client"

export const sortMethods = [
	{ value: "", label: "Sort by" },
	{ value: "rating", label: "Rating" },
	{ value: "low-high", label: "Price, low to high" },
	{ value: "high-low", label: "Price, high to low" },
	{ value: "a-z", label: "Alphabetically, A-Z" },
	{ value: "z-a", label: "Alphabetically, Z-A" },
]

export const sort = (products: Product[], sortBy: string) => {
	if (sortBy === "") {
		return products
	}
	if (sortBy === "rating") {
		return products
	}
	if (sortBy === "a-z") {
		return sortAZ(products)
	}
	if (sortBy === "z-a") {
		return sortZA(products)
	}
	if (sortBy === "low-high") {
		return sortByPriceLow(products)
	}
	if (sortBy === "high-low") {
		return sortByPriceHigh(products)
	}
}

// TODO : recursive function to sort better

export const sortAZ = (products: Product[]) => {
	return [...products].sort((p1, p2) =>
		p1.name[0] > p2.name[0] ? 1 : p1.name[0] < p2.name[0] ? -1 : 0,
	)
}

export const sortZA = (products: Product[]) => {
	return [...products].sort((p1, p2) =>
		p1.name[0] < p2.name[0] ? 1 : p1.name[0] > p2.name[0] ? -1 : 0,
	)
}

export const sortByPriceLow = (products: Product[]) => {
	return [...products].sort((p1, p2) => (p1.price > p2.price ? 1 : p1.price < p2.price ? -1 : 0))
}

export const sortByPriceHigh = (products: Product[]) => {
	return [...products].sort((p1, p2) => (p1.price < p2.price ? 1 : p1.price > p2.price ? -1 : 0))
}

export const sortByRating = (products: Product[]) => {
	return [...products]
	// .sort((p1, p2) =>
	// 	p1.ratings < p2.ratings ? 1 : p1.ratings > p2.rating ? -1 : 0,
	// )
}
