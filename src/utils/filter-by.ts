import { Product } from "@prisma/client"

import { SelectedFilters } from "../types"

export const filterProducts = (
	products: Product[],
	menuCategory: string,
	selectedFilters: SelectedFilters,
	searchWord = "",
) => {
	const { colors, minPrice, maxPrice, availability } = selectedFilters

	return products
		.filter((product) => product.name.toLowerCase().includes(searchWord.toLowerCase()))
		.filter((product) =>
			menuCategory === "shopall" || menuCategory === ""
				? product
				: product.categories.includes(menuCategory),
		)
		.filter((product) =>
			colors.some((color) => product.variants.map((variant) => variant.color).includes(color)),
		)
		.filter((product) => product.price >= minPrice && product.price <= maxPrice)
		.filter((product) =>
			availability === "all"
				? product
				: availability === "in-stock"
				? product.instock > 0
				: product.instock === 0,
		)
}
