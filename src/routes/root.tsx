import { Outlet } from "react-router-dom"

import { CartModal, WishlistModal } from "@/features"
import { Footer } from "@/layouts/footer"
import { Header } from "@/layouts/header"
import { useCartStore } from "@/stores"

export function Root() {
	const cartOpened = useCartStore((state) => state.opened)

	return (
		<div className={`${cartOpened ? "fixed" : ""} flex h-full flex-col`}>
			<Header />
			<Outlet />
			<CartModal />
			<WishlistModal />
			<Footer />
		</div>
	)
}
