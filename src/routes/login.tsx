import { useEffect } from "react"
import { NavLink, useNavigate } from "react-router-dom"

import { useAuthStore } from "@/stores"
import { trpc } from "@/utils"

export function Login() {
	const navigate = useNavigate()
	const isAuth = useAuthStore((state) => state.auth)
	const setAuth = useAuthStore((state) => state.setAuth)

	const login = trpc.users.login.useMutation({
		onMutate: () => {
			utils.users.current.cancel()
		},
		onSuccess: () => {
			utils.users.current.invalidate()
			navigate("/")
			setAuth(true)
		},
	})
	const utils = trpc.useContext()

	const handleLogin = (e: React.FormEvent<HTMLFormElement>) => {
		e.preventDefault()
		const target = e.target as typeof e.target & {
			email: { value: string }
			password: { value: string }
		}
		const email = target.email.value
		const password = target.password.value
		login.mutate({ email: email, password: password })
	}

	useEffect(() => {
		if (isAuth) navigate("/")
	})

	return (
		<div className="mx-4 my-10 flex justify-center">
			<div className="flex max-w-xl flex-col items-center px-20 pt-6 pb-12 lg:w-1/2 ">
				<div className="mb-4 text-3xl font-bold">Login</div>
				<NavLink to="/register" className="mb-10 text-sm text-slate-700 underline">
					Don&apos;t have an account? Sign up here.
				</NavLink>
				<form onSubmit={(e) => handleLogin(e)} className="w-full">
					<div className="flex flex-col gap-4">
						<label htmlFor="email">
							<input
								type="email"
								name="email"
								id="email"
								placeholder="Email..."
								required
								className="h-10 w-full rounded-full border px-4"
							/>
						</label>
						<label htmlFor="password">
							<input
								type="password"
								name="password"
								id="password"
								placeholder="Password..."
								required
								className="h-10 w-full rounded-full border px-4"
							/>
						</label>
						<div className="text-sm text-red-800">{login.isError ? login.error.message : ""}</div>
					</div>
					<div className="mt-12 flex items-center justify-between">
						<button type="submit" className="rounded-full bg-slate-900 px-6 py-2 text-white">
							Sign In
						</button>
						<NavLink to="/" className="text-sm underline">
							Return to Store
						</NavLink>
					</div>
				</form>
			</div>
			<div></div>
		</div>
	)
}
