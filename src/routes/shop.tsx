import { Product, Variant } from "@prisma/client"
import { useEffect, useRef, useState } from "react"

import { Filters, FiltersMenu, ProductsList } from "@/features"
import { MenuBar } from "@/layouts/menubar"
import type { SelectedFilters } from "@/types"
import { trpc } from "@/utils"

export function Shop() {
	const [isFilterOpened, setIsFilterOpened] = useState(false)
	const shopRef = useRef<HTMLDivElement>(null)

	// TODO: Persist searchword & sortBy in the URL
	const [searchWord, setSearchWord] = useState("")
	const [sortBy, setSortBy] = useState("")
	const [menuCategory, setMenuCategory] = useState("")
	const [filter, setFilter] = useState(false)

	const getProducts = trpc.products.getProducts.useQuery()
	const products = getProducts.data

	const initialFilterValues = {
		allColors: [
			...new Set(
				products
					?.map((product: Product) => product.variants.map((variant: Variant) => variant.color))
					.flat(),
			),
		] || [""],
		minPrice: products ? Math.min(...products.map((product: Product) => product.price)) : 0,
		maxPrice: products ? Math.max(...products.map((product: Product) => product.price)) : 0,
	}
	const [selectedFilters, setSelectedFilters] = useState<SelectedFilters>({
		colors: initialFilterValues.allColors,
		minPrice: initialFilterValues.minPrice,
		maxPrice: initialFilterValues.maxPrice,
		availability: "all",
	})

	useEffect(() => {
		setSelectedFilters({
			colors: initialFilterValues.allColors,
			minPrice: initialFilterValues.minPrice,
			maxPrice: initialFilterValues.maxPrice,
			availability: "all",
		})
	}, [products])

	return (
		<main className="h-full">
			<MenuBar
				shopRef={shopRef}
				menuCategory={menuCategory}
				setMenuCategory={setMenuCategory}
				setFilter={setFilter}
			/>
			<div className="relative max-h-96 bg-slate-100">
				<img
					src="/images/countryside.jpg"
					alt="countryside"
					className="max-h-96 w-full object-cover"
				/>
				<div className="absolute top-0  flex h-full w-full items-end justify-center pb-10">
					<button
						onClick={() => (shopRef.current ? shopRef.current.scrollIntoView() : null)}
						className="flex items-center gap-2 rounded-full border border-slate-900 bg-white stroke-black px-5 py-2"
					>
						<img src="/icons/arrow-down.svg" alt="arrow-down" />

						<span>Go to shop</span>
					</button>
				</div>
			</div>

			<div ref={shopRef}>
				<FiltersMenu
					isFilterOpened={isFilterOpened}
					setIsFilterOpened={setIsFilterOpened}
					setSearchWord={setSearchWord}
					setSortBy={setSortBy}
					setFilter={setFilter}
				/>
				<div className="relative flex h-full grid-cols-2 flex-col ">
					<div className="flex h-full">
						<Filters
							isFilterOpened={isFilterOpened}
							selectedFilters={selectedFilters}
							setSelectedFilters={setSelectedFilters}
							initialFilterValues={initialFilterValues}
							setFilter={setFilter}
						/>
						<ProductsList
							searchWord={searchWord}
							sortBy={sortBy}
							menuCategory={menuCategory}
							selectedFilters={selectedFilters}
							filter={filter}
						/>
					</div>
				</div>
			</div>
		</main>
	)
}
