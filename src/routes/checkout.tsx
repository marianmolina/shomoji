import { useEffect } from "react"
import { useNavigate } from "react-router-dom"

import { useAuthStore, useCartStore } from "@/stores"
import { getTotal } from "@/utils"

export function Checkout() {
	const navigate = useNavigate()
	const isAuth = useAuthStore((state) => state.auth)

	const cart = useCartStore((state) => state.cart)
	const resetCart = useCartStore((state) => state.resetCart)
	const { totalFormatted, nbItems } = getTotal(cart)

	const goToShop = () => {
		resetCart()
		navigate("/")
	}

	useEffect(() => {
		if (!isAuth || cart.length === 0) navigate("/")
	})

	return (
		<div className={` flex h-full flex-col`}>
			<div className=" flex flex-wrap-reverse items-center gap-10 px-10 pt-14 pb-20">
				<div className="grow px-10">
					<div className="mx-auto flex h-fit w-full max-w-xl flex-col items-center	">
						<div className="mb-8 flex items-center gap-8">
							Today it&apos;s on us, click on the button to use our credit card!{" "}
							<button className="font-bold text-slate-800 underline decoration-slate-800 decoration-2 underline-offset-4">
								Use Card
							</button>
						</div>
						<form className="flex w-full max-w-sm flex-col text-sm text-slate-800">
							<label htmlFor="card" className="mb-3 flex flex-col gap-1">
								Card
								<input type="text" name="card" id="card" className="h-8 max-w-xs rounded border" />
							</label>
							<label htmlFor="name" className="mb-3 flex flex-col gap-1">
								Name on card
								<input type="text" name="name" id="name" className="h-8 max-w-xs rounded border" />
							</label>
							<label htmlFor="country" className="mb-3 flex flex-col gap-1">
								Country
								<input
									type="text"
									name="country"
									id="country"
									className="h-8 max-w-xs rounded border"
								/>
							</label>
							<button
								type="submit"
								onClick={goToShop}
								className="mt-4 max-w-xs rounded bg-slate-800 py-2 text-base text-slate-100"
							>
								Pay {totalFormatted}€
							</button>
						</form>
					</div>
				</div>
				<div className="grow ">
					<div className="mx-auto h-fit w-full max-w-lg rounded-lg border bg-slate-50 px-10 py-8">
						<div className="mb-4 flex justify-between gap-12 text-lg font-black">
							<span>Order summary</span> <span>{nbItems} Item(s)</span>
						</div>
						<hr className="border-gray-300" />
						<div className="flex flex-col gap-4 py-6">
							{cart.map((item, index) => (
								<div key={index} className="flex justify-between gap-4">
									<span>
										{item.name} ({item.amount})
									</span>{" "}
									<span>{item.totalPrice}€</span>
								</div>
							))}
						</div>
						<div className="mt-4 mb-2 flex justify-between gap-12 text-lg font-black">
							<span>Order total</span> <span>{totalFormatted}€</span>
						</div>
						<hr className="border-gray-300" />
					</div>
				</div>
			</div>
		</div>
	)
}
