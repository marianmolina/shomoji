export type InitialFilterValues = {
	allColors: string[]
	minPrice: number
	maxPrice: number
}

export type SelectedFilters = {
	colors: string[]
	minPrice: number
	maxPrice: number
	availability: "all" | "in-stock" | "out-stock"
}

export type CartProduct = {
	name: string
	color: string
	price: number
	amount: number
	totalPrice: number
	image: string
}
