import { Product } from "@prisma/client"
import parse from "html-react-parser"
import { useState } from "react"

import { useCartStore, useWishStore } from "@/stores"
import { trpc } from "@/utils"

export function ProductItem({ product }: { product: Product }) {
	const { name, price, variants, ratings, instock } = product
	const [selectedColor, setSelectedColor] = useState("")

	const addToCart = useCartStore((state) => state.addToCart)
	const wishlist = useWishStore((state) => state.wishlist)
	const addToWish = useWishStore((state) => state.addToWish)

	const currentRating = Math.round(
		ratings.reduce((total: number, currentValue: number) => total + currentValue) /
			(ratings.length - 1),
	)

	const utils = trpc.useContext()
	const updateRating = trpc.products.updateRating.useMutation({
		onMutate: () => {
			utils.products.getProducts.cancel()
			const optimisticUpdate = utils.products.getProducts.getData()
			// console.log(optimisticUpdate)
			if (optimisticUpdate) {
				utils.products.getProducts.setData(undefined, optimisticUpdate)
			}
		},
		onSettled: () => {
			utils.products.getProducts.invalidate()
		},
	})

	return (
		<div className="relative overflow-hidden whitespace-nowrap rounded border bg-white">
			<div className="flex h-48 w-48 items-center bg-slate-100">
				{variants.length > 1 ? (
					<div className="h-24 w-full items-center justify-center [&>svg]:h-full [&>svg]:w-full">
						{selectedColor
							? parse(variants.filter((variant) => variant.color === selectedColor)[0].image)
							: parse(variants[0].image)}
					</div>
				) : (
					<div className="h-24 w-full items-center justify-center  [&>svg]:h-full [&>svg]:w-full">
						{parse(variants[0].image)}
					</div>
				)}
			</div>

			<ul className="flex gap-2 border-y py-2 px-4">
				{variants.map((variant) => (
					<li key={variant.color}>
						<button
							title={variant.color}
							style={{ backgroundColor: variant.color }}
							onClick={() => {
								setSelectedColor(variant.color)
							}}
							className={`${
								selectedColor === variant.color
									? "border-slate-300 ring-slate-400"
									: "ring-slate-300"
							} relative flex h-5 w-5 items-center justify-center rounded-full border-[3px] border-white ring-1  hover:ring-slate-500`}
						/>
					</li>
				))}
			</ul>
			<div className="flex w-48 flex-col gap-y-3 p-4">
				<div className="flex items-center justify-between gap-2">
					<h1 title={name} className=" overflow-hidden text-ellipsis font-semibold">
						{name}
					</h1>
					<span className="text-sm">{price}€</span>
				</div>
				<div className="flex items-center justify-between">
					<div className="flex flex-row-reverse justify-end ">
						{[...Array(5)].map((item, index) => (
							<button
								key={index}
								onClick={() => {
									updateRating.mutate({ product, rating: 5 - index })
								}}
								style={{ opacity: currentRating >= 5 - index ? 1 : 0.1 }}
								className="peer fill-black py-1 px-0.5 opacity-10 hover:opacity-100 peer-hover:opacity-100"
							>
								<svg
									width="15"
									height="15"
									viewBox="0 0 15 15"
									fill="inherit"
									xmlns="http://www.w3.org/2000/svg"
								>
									<title>{5 - index} star</title>
									<path
										fillRule="evenodd"
										clipRule="evenodd"
										d="M6.58253 0.611455C6.92166 -0.203818 8.07834 -0.203818 8.41747 0.611455L9.99351 4.40168L14.0843 4.72946C14.9654 4.79986 15.3227 5.899 14.6512 6.47431L11.5347 9.1442L12.4863 13.1358C12.6914 13.9957 11.7565 14.6747 11.0026 14.2145L7.5 12.0752L3.99743 14.2145C3.24347 14.6747 2.30859 13.995 2.51374 13.1358L3.46527 9.1442L0.348756 6.47431C-0.32269 5.899 0.0346074 4.79986 0.915739 4.72946L5.00649 4.40168L6.58253 0.612212V0.611455Z"
										fill="inherit"
									/>
								</svg>
							</button>
						))}
					</div>
					<span className="text-xs">{ratings.length - 1} reviews</span>
				</div>
				{instock === 0 ? (
					<span className="mt-4 flex h-11 items-center justify-center text-sm text-slate-500">
						Out of Stock
					</span>
				) : (
					<button
						onClick={() => addToCart(product, selectedColor)}
						className="mx-auto mt-4 w-5/6 rounded-full border border-slate-900 p-2 font-semibold hover:bg-slate-900 hover:text-white"
					>
						Add to cart
					</button>
				)}
			</div>
			<button
				onClick={() => addToWish(product)}
				className={`${
					wishlist?.map((p: Product) => p.name).includes(product.name)
						? "fill-red-600 stroke-none"
						: "fill-none stroke-slate-500"
				} absolute top-0 right-0 m-3 flex h-10 w-10 items-center justify-center rounded-full bg-white  hover:fill-red-600 hover:stroke-none`}
			>
				<svg
					width="20"
					height="18"
					viewBox="0 0 16 14"
					fill="inherit"
					xmlns="http://www.w3.org/2000/svg"
				>
					<title id="heart-title">Heart</title>
					<desc id="heart-description">The shape of a heart</desc>
					<path
						d="M14.75 4.1875C14.75 2.32375 13.1758 0.8125 11.234 0.8125C9.78275 0.8125 8.53625 1.657 8 2.86225C7.46375 1.657 6.21725 0.8125 4.76525 0.8125C2.825 0.8125 1.25 2.32375 1.25 4.1875C1.25 9.6025 8 13.1875 8 13.1875C8 13.1875 14.75 9.6025 14.75 4.1875Z"
						stroke="inherit"
						strokeOpacity="0.5"
						strokeWidth="1.5"
						strokeLinecap="round"
						strokeLinejoin="round"
					/>
				</svg>
			</button>
		</div>
	)
}
