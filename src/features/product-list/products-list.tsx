import { Product } from "@prisma/client"
import { useEffect, useState } from "react"

import { WishlistButton } from "@/features/wishlist/wishlist-button"
import type { SelectedFilters } from "@/types"
import { filterProducts, sort, trpc } from "@/utils"

import { ProductItem } from "./product-item"

const PRODUCTS_PER_PAGE = 10
const MAX_PAGES_NUMBER = 10

export function ProductsList({
	searchWord,
	sortBy,
	menuCategory,
	selectedFilters,
	filter,
}: {
	searchWord: string
	sortBy: string
	menuCategory: string
	selectedFilters: SelectedFilters
	filter: boolean
}) {
	const { data, isError, error } = trpc.products.getProducts.useQuery()
	if (isError) throw new Error(error.message)
	const products = data

	// SORT AND FILTER
	const sortedProducts = products ? sort(products, sortBy) : null
	const filteredProducts =
		filter && sortedProducts
			? filterProducts(sortedProducts, menuCategory, selectedFilters, searchWord)
			: products

	// TODO : Persist currentPage in URL
	// PAGINATION
	const productsNb = filteredProducts ? filteredProducts.length : products?.length || 0
	const totalPages = Math.ceil(productsNb / PRODUCTS_PER_PAGE)
	const [currentPage, setCurrentPage] = useState(0)
	const startPage = Math.max(
		0,
		Math.min(totalPages - MAX_PAGES_NUMBER, currentPage - MAX_PAGES_NUMBER / 2),
	)
	const endPage = startPage + MAX_PAGES_NUMBER
	const isFirstPage = currentPage === 0
	const isLastPage = currentPage === totalPages - 1

	useEffect(() => {
		setCurrentPage(0)
	}, [totalPages])

	if (!products) return <div className="grow py-5 px-10">Loading...</div>
	return (
		<div className="grow py-5 px-10">
			<div className="flex justify-between">
				<span className="text-gray-500 ">
					{filteredProducts
						? filteredProducts.length > 1
							? filteredProducts.length + " products"
							: filteredProducts.length + " product"
						: products
						? products.length + " products"
						: "0 product"}
				</span>
				{totalPages > 0 ? (
					<div className="flex justify-center gap-2 text-xs text-gray-400 ">
						<button
							onClick={() => {
								setCurrentPage((page) => page - 1)
							}}
							disabled={isFirstPage ? true : false}
							className={`${isFirstPage ? "opacity-25" : ""} w-14  py-2  text-gray-600`}
						>
							{"<"}
						</button>
						<div>
							{[...Array(totalPages).keys()].slice(startPage, endPage).map((page) => (
								<button
									key={page}
									onClick={() => {
										setCurrentPage(page)
									}}
									className={`${
										currentPage === page ? "border-slate-900 text-slate-900" : ""
									} w-14 py-2`}
								>
									{page + 1}
								</button>
							))}
						</div>
						<button
							onClick={() => {
								setCurrentPage((page) => page + 1)
							}}
							disabled={isLastPage ? true : false}
							className={`${isLastPage ? "opacity-25" : ""} w-14  py-2  text-gray-600`}
						>
							{">"}
						</button>
					</div>
				) : null}
			</div>

			<ul className="mt-8 flex flex-wrap justify-start gap-8 ">
				{filteredProducts
					? filteredProducts
							.slice(currentPage * PRODUCTS_PER_PAGE, (currentPage + 1) * PRODUCTS_PER_PAGE)
							.map((product: Product) => (
								<li key={product.slug}>
									<ProductItem product={product} />
								</li>
							))
					: sortedProducts
							?.slice(currentPage * PRODUCTS_PER_PAGE, (currentPage + 1) * PRODUCTS_PER_PAGE)
							.map((product) => (
								<li key={product.slug}>
									<ProductItem product={product} />
								</li>
							))}
			</ul>
			{totalPages > 0 ? (
				<div className="mt-20 mb-10 flex justify-center gap-8 text-slate-600">
					<button
						onClick={() => {
							setCurrentPage((page) => page - 1)
						}}
						disabled={isFirstPage ? true : false}
						className={`${isFirstPage ? "opacity-50" : ""} w-14 py-2`}
					>
						{"<"}
					</button>
					<div>
						{[...Array(totalPages).keys()].slice(startPage, endPage).map((page) => (
							<button
								key={page}
								onClick={() => {
									setCurrentPage(page)
								}}
								className={`${
									currentPage === page ? "border-slate-900 bg-slate-100 text-slate-900" : ""
								} w-14 py-2`}
							>
								{page + 1}
							</button>
						))}
					</div>

					<button
						onClick={() => {
							setCurrentPage((page) => page + 1)
						}}
						disabled={isLastPage ? true : false}
						className={`${isLastPage ? "opacity-50" : ""} w-14  py-2`}
					>
						{">"}
					</button>
				</div>
			) : null}

			<WishlistButton />
		</div>
	)
}
