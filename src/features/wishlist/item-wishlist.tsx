import { Product } from "@prisma/client"
import parse from "html-react-parser"
import { useState } from "react"

import { useWishStore } from "@/stores/wishlist-store"

export function WishlistItem({ product, wishlist }: { product: Product; wishlist: Product[] }) {
	const { name, price, variants } = product
	const [selectedColor, setSelectedColor] = useState("")

	const removeWish = useWishStore((state) => state.addToWish)

	return (
		<div className="flex gap-5 p-10">
			<div>
				{variants.length > 1 ? (
					<div className="flex h-32 w-32 rounded border bg-slate-100 p-4 [&>svg]:h-full [&>svg]:w-full ">
						{wishlist.length > 0
							? selectedColor
								? parse(variants.filter((variant) => variant.color === selectedColor)[0].image)
								: parse(variants[0].image)
							: ""}
					</div>
				) : (
					<div className="flex h-32 w-32 rounded border bg-slate-100 p-4 [&>svg]:h-full [&>svg]:w-full">
						{parse(variants[0].image)}
					</div>
				)}

				<ul className="flex justify-center gap-2 border-b  py-2">
					{variants.map((variant) => (
						<li key={variant.color}>
							<button
								title={variant.color}
								style={{ backgroundColor: variant.color }}
								onClick={() => {
									setSelectedColor(variant.color)
								}}
								className={`${
									selectedColor === variant.color
										? "border-slate-300 ring-slate-400"
										: "ring-slate-300"
								} relative flex h-5 w-5 items-center justify-center rounded-full border-[3px] border-white ring-1  hover:ring-slate-500`}
							/>
						</li>
					))}
				</ul>
			</div>

			<div className="flex w-48 flex-col justify-between gap-y-3  p-4">
				<div>
					<h1 title={name} className=" overflow-hidden text-ellipsis font-semibold">
						{name}
					</h1>
					<span className="text-sm">{price}€</span>
				</div>
				<button
					onClick={() => removeWish(product)}
					className=" text-left text-xs text-slate-600 underline"
				>
					Remove
				</button>
			</div>
		</div>
	)
}
