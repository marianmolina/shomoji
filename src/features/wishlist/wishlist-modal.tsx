import { Product } from "@prisma/client"

import { useWishStore } from "@/stores/wishlist-store"

import { WishlistItem } from "./item-wishlist"

export function WishlistModal() {
	const wishlist = useWishStore((state) => state.wishlist)
	const opened = useWishStore((state) => state.opened)
	const closeWishlist = useWishStore((state) => state.toggleWish)

	return (
		<dialog
			open={opened}
			className="fixed z-10 mx-0 h-full w-full justify-end overflow-y-scroll bg-black bg-opacity-50 p-0"
		>
			<div className="ml-auto flex h-fit min-h-full w-fit min-w-[425px] flex-col  bg-white">
				<div className="flex items-center justify-between gap-2 border-b py-5 px-10">
					<span className="text-lg uppercase">Wishlist</span>
					<button onClick={closeWishlist}>
						<img src="/icons/cross.svg" alt="cross" className="w-3" />
					</button>
				</div>
				<div className="">
					{wishlist?.length > 0 ? (
						wishlist?.map((product: Product, index: number) => (
							<WishlistItem key={index} product={product} wishlist={wishlist} />
						))
					) : (
						<div className="mt-10 text-center text-slate-500">Your wishlist is empty</div>
					)}
				</div>
			</div>
		</dialog>
	)
}
