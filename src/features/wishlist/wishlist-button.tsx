import { useWishStore } from "@/stores"

export function WishlistButton() {
	const openWishlist = useWishStore((state) => state.toggleWish)
	const wishlist = useWishStore((state) => state.wishlist)

	return (
		<button
			title="Wishlist"
			onClick={openWishlist}
			className="
				 sticky bottom-10 mr-2 ml-auto flex h-14 items-center justify-center gap-4 rounded-full bg-white stroke-none py-4 px-6 text-sm shadow hover:bg-slate-50"
		>
			<img src="/icons/heart-red.svg" alt="red-heart" className="h-5 w-5" />
			Wishlist ({wishlist.length})
		</button>
	)
}
