import { useEffect } from "react"
import { NavLink, useNavigate } from "react-router-dom"

import { useAuthStore } from "@/stores"
import { trpc } from "@/utils"

export function Credentials() {
	const navigate = useNavigate()
	const utils = trpc.useContext()
	const getUser = trpc.users.current.useQuery()
	const user = getUser.data
	const isAuth = useAuthStore((state) => state.auth)
	const setAuth = useAuthStore((state) => state.setAuth)
	const logout = trpc.users.logout.useMutation({
		onSuccess: () => {
			utils.users.current.invalidate()
			setAuth(false)
		},
	})

	useEffect(() => {
		if (!isAuth) navigate("/")
	})

	return (
		<div className="mx-4 my-10 flex justify-center">
			<div className="flex max-w-xl flex-col items-center px-20 pt-6 pb-12 sm:w-2/3 lg:w-1/2 ">
				<div className="mb-4 text-3xl font-bold">Credentials</div>
				<form className="w-full">
					<div className="flex flex-col gap-4">
						<label htmlFor="email">
							<input
								type="text"
								name="email"
								id="email"
								value={user?.email}
								readOnly
								className="h-10 w-full rounded-full bg-gray-100 px-4 text-slate-600"
							/>
						</label>
						<label htmlFor="password">
							<input
								type="password"
								name="password"
								id="password"
								placeholder="Password (8 characters minimum)"
								minLength={8}
								required
								className="h-10 w-full rounded-full border px-4 placeholder:text-sm"
							/>
						</label>

						<div className="text-sm text-red-800">Error message</div>
					</div>
					<div className="mt-12 flex items-center justify-between">
						<div>
							<button type="submit" className="mr-4 rounded-full bg-slate-900 px-6 py-2 text-white">
								Update
							</button>
							<NavLink
								to="/"
								onClick={() => logout.mutate()}
								className="rounded-full border px-6 py-2"
							>
								Log Out
							</NavLink>
						</div>
						<NavLink to="/" className="text-sm underline">
							Return to Store
						</NavLink>{" "}
					</div>
				</form>
			</div>
			<div></div>
		</div>
	)
}
