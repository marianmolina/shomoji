import { useCartStore, useLocaleStore } from "@/stores"
import { getTotal } from "@/utils"

export function CartButtonDark() {
	const translation = useLocaleStore((state) => state.translation)

	const cart = useCartStore((state) => state.cart)

	const openCart = useCartStore((state) => state.toggleCart)
	const { totalFormatted, nbItems } = getTotal(cart)

	return (
		<button
			onClick={openCart}
			className="relative flex items-center rounded-full border border-slate-900 bg-slate-900 px-5 py-2 text-white"
		>
			{translation.navbar.cart}:
			<span className="h-6 w-24 overflow-hidden text-clip text-end">
				{totalFormatted}€ ({nbItems})
			</span>
		</button>
	)
}
