import { useNavigate } from "react-router-dom"

import { useAuthStore, useCartStore } from "@/stores"
import type { CartProduct } from "@/types"
import { getTotal } from "@/utils"

import { ItemCart } from "./item-cart"

export function CartModal() {
	const isAuth = useAuthStore((state) => state.auth)

	const cart = useCartStore((state) => state.cart)
	const opened = useCartStore((state) => state.opened)
	const closeCart = useCartStore((state) => state.toggleCart)
	const { totalFormatted } = getTotal(cart)

	const navigate = useNavigate()
	const goToCheckOut = (cart: CartProduct[]) => {
		if (cart.length > 0) {
			if (isAuth) {
				closeCart()
				navigate("/checkout")
			}
		}
	}
	const goToLogin = () => {
		closeCart()
		navigate("/login")
	}

	return (
		<dialog
			open={opened}
			className="fixed z-10 mx-0 h-full w-full justify-end overflow-y-scroll bg-black bg-opacity-50 p-0"
		>
			<div className="ml-auto flex h-fit min-h-full w-fit min-w-[415px] flex-col  bg-white ">
				<div className="flex items-center justify-between gap-2 border-b py-5 px-10">
					<span className="text-lg uppercase">Cart</span>
					<button onClick={closeCart}>
						<img src="/icons/cross.svg" alt="cross" className="w-3" />
					</button>
				</div>
				<div className="mb-10 grow">
					{cart?.length > 0 ? (
						cart?.map((product: CartProduct, index: number) => (
							<ItemCart key={index} product={product} />
						))
					) : (
						<div className="mt-10 text-center text-slate-500">Your cart is empty</div>
					)}
				</div>
				<div className=" bg-slate-100 px-10 py-8">
					<div className="">Total</div>
					<div className="mt-4 mb-10 text-4xl">{totalFormatted}€</div>
					{isAuth ? (
						<button
							onClick={() => goToCheckOut(cart)}
							className="block w-full rounded-full border border-slate-900 py-2 text-lg font-semibold hover:bg-slate-900 hover:text-white"
						>
							Check Out
						</button>
					) : (
						<button
							onClick={() => goToLogin()}
							className="block w-full rounded-full border border-slate-900 py-2 text-lg font-semibold hover:bg-slate-900 hover:text-white"
						>
							Login to checkout
						</button>
					)}
				</div>
			</div>
		</dialog>
	)
}
