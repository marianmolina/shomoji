import parse from "html-react-parser"

import { useCartStore } from "@/stores"
import type { CartProduct } from "@/types"

export function ItemCart({ product }: { product: CartProduct }) {
	const updateAmount = useCartStore((state) => state.updateAmount)
	const removeFromCart = useCartStore((state) => state.removeFromCart)

	return (
		<div className="flex gap-10 p-10">
			<div className="flex h-32 w-32 rounded border bg-slate-100 p-4 [&>svg]:h-full [&>svg]:w-full">
				{parse(product.image)}
			</div>

			<div>
				<h1>{product.name}</h1>
				<div className="text-sm text-slate-600">{product.color}</div>
				<div className="mt-2 text-sm text-slate-600">{product.totalPrice}€</div>
				<div className="mt-2 flex justify-between gap-8">
					<div className="flex items-center justify-between rounded-full border ">
						<button onClick={() => updateAmount(product, "decrement")} className="py-3 px-4">
							<img alt="minus" src="/icons/minus.svg" />
						</button>
						<span>{product.amount}</span>
						<button onClick={() => updateAmount(product, "increment")} className=" py-3 px-4">
							<img alt="plus" src="/icons/plus.svg" />
						</button>
					</div>
					<button
						onClick={() => removeFromCart(product)}
						className="text-xs text-slate-600 underline"
					>
						Remove
					</button>
				</div>
			</div>
		</div>
	)
}
