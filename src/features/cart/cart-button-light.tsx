import { useCartStore, useLocaleStore } from "@/stores"
import { getTotal } from "@/utils"

export function CartButtonLight() {
	const translation = useLocaleStore((state) => state.translation)

	const cart = useCartStore((state) => state.cart)
	const openCart = useCartStore((state) => state.toggleCart)

	const { totalFormatted, nbItems } = getTotal(cart)

	return (
		<button
			onClick={openCart}
			className="relative flex items-center fill-black stroke-black px-5 py-4 text-black"
		>
			{translation.navbar.cart}:
			<span className="w-24 text-end">
				{totalFormatted}€ ({nbItems})
			</span>
			<div
				className={`${
					nbItems > 0 ? "visible" : "hidden"
				} absolute top-4 right-2 h-2 w-2 rounded-full bg-red-500`}
			/>
		</button>
	)
}
