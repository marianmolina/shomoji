import { useState } from "react"

import type { InitialFilterValues, SelectedFilters } from "@/types"

export function Filters({
	isFilterOpened,
	selectedFilters,
	setSelectedFilters,
	initialFilterValues,
	setFilter,
}: {
	isFilterOpened: boolean
	selectedFilters: SelectedFilters
	setSelectedFilters: React.Dispatch<React.SetStateAction<SelectedFilters>>
	initialFilterValues: InitialFilterValues
	setFilter: React.Dispatch<React.SetStateAction<boolean>>
}) {
	// open/close icons
	const [colOpened, setColOpened] = useState(false)
	const [priceOpened, setPriceOpened] = useState(false)
	const [avOpened, setAvOpened] = useState(false)

	const { allColors, minPrice, maxPrice } = initialFilterValues

	return (
		<aside
			className={`${
				isFilterOpened
					? "sticky top-24 flex h-screen min-w-[275px] flex-col gap-4 border-r bg-white px-10 py-6"
					: "hidden"
			} `}
		>
			<details>
				<summary
					onClick={() => setColOpened((value) => !value)}
					className="flex w-full justify-between gap-2 hover:cursor-pointer"
				>
					Color
					{colOpened ? (
						<img src="/icons/minimize.svg" alt="minimize" />
					) : (
						<img src="/icons/maximize.svg" alt="maximize" />
					)}
				</summary>
				<div className="flex max-w-[175px] flex-wrap gap-2 py-4">
					{allColors?.map((color, index) => (
						<button
							key={index}
							style={{ backgroundColor: color }}
							title={color.toString()}
							onClick={() => {
								selectedFilters.colors.includes(color)
									? setSelectedFilters({
											...selectedFilters,
											colors: selectedFilters.colors.filter((item) => item !== color),
									  })
									: setSelectedFilters({
											...selectedFilters,
											colors: [...new Set([...selectedFilters.colors, color])],
									  })
								setFilter(true)
							}}
							className={`${
								selectedFilters.colors.includes(color)
									? "ring-slate-600"
									: "opacity-40 outline-slate-400"
							}  h-6 w-6 rounded-full border-4 border-white ring-1 ring-slate-300 hover:ring-slate-500`}
						></button>
					))}
				</div>
				<div className="mb-4 flex gap-10 text-xs text-gray-700 underline">
					<button
						onClick={() => {
							setSelectedFilters({
								...selectedFilters,
								colors: allColors,
							})
							setFilter(true)
						}}
					>
						Select all
					</button>
					<button
						onClick={() => {
							setSelectedFilters({
								...selectedFilters,
								colors: [],
							})
							setFilter(true)
						}}
					>
						Unselect all
					</button>
				</div>
			</details>
			<details>
				<summary
					onClick={() => setPriceOpened((value) => !value)}
					className="flex w-full justify-between gap-2  hover:cursor-pointer"
				>
					Price
					{priceOpened ? (
						<img src="/icons/minimize.svg" alt="minimize" />
					) : (
						<img src="/icons/maximize.svg" alt="maximize" />
					)}
				</summary>
				<div className="flex items-center justify-between gap-4 py-4 text-sm">
					<div className="flex w-16 justify-between gap-1 rounded border px-3 py-2">
						<span>€</span>
						<input
							type="text"
							defaultValue={minPrice?.toString() || "0"}
							onChange={(e) => {
								setSelectedFilters({ ...selectedFilters, minPrice: parseInt(e.target.value) | 0 })
								setFilter(true)
							}}
							className="w-7 text-right text-gray-700"
						/>
					</div>
					<span>-</span>
					<div className="flex w-16 justify-between gap-1 rounded border  px-3 py-2">
						<span>€</span>
						<input
							type="text"
							defaultValue={maxPrice?.toString() || "0"}
							onChange={(e) =>
								setSelectedFilters({ ...selectedFilters, maxPrice: parseInt(e.target.value) | 0 })
							}
							className="w-7 text-right text-gray-700"
						/>
					</div>
				</div>
			</details>
			<details>
				<summary
					onClick={() => setAvOpened((value) => !value)}
					className="flex w-full justify-between gap-2  hover:cursor-pointer"
				>
					Availability
					{avOpened ? (
						<img src="/icons/minimize.svg" alt="minimize" />
					) : (
						<img src="/icons/maximize.svg" alt="maximize" />
					)}
				</summary>
				<div className="py-4">
					<div className="flex items-center gap-4 py-1 text-sm ">
						<input
							type="radio"
							id="all"
							name="availability"
							value="all"
							onClick={() => {
								setSelectedFilters({ ...selectedFilters, availability: "all" })
								setFilter(true)
							}}
							defaultChecked
						></input>
						<label htmlFor="all">All</label>
					</div>
					<div className="flex items-center gap-4 py-1 text-sm ">
						<input
							type="radio"
							id="in-stock"
							name="availability"
							value="in-stock"
							onClick={() => {
								setSelectedFilters({ ...selectedFilters, availability: "in-stock" })
								setFilter(true)
							}}
						></input>
						<label htmlFor="in-stock">In stock</label>
					</div>

					<div className="flex items-center gap-4 py-1 text-sm ">
						<input
							type="radio"
							id="out-stock"
							name="availability"
							value="out-stock"
							onClick={() => {
								setSelectedFilters({ ...selectedFilters, availability: "out-stock" })
								setFilter(true)
							}}
						></input>
						<label htmlFor="out-stock">Out of stock</label>
					</div>
				</div>
			</details>
		</aside>
	)
}
