import { sortMethods } from "@/utils/sort-by"

export function FiltersMenu({
	isFilterOpened,
	setIsFilterOpened,
	setSearchWord,
	setSortBy,
	setFilter,
}: {
	isFilterOpened: boolean
	setIsFilterOpened: React.Dispatch<React.SetStateAction<boolean>>
	setSearchWord: React.Dispatch<React.SetStateAction<string>>
	setSortBy: React.Dispatch<React.SetStateAction<string>>
	setFilter: React.Dispatch<React.SetStateAction<boolean>>
}) {
	return (
		<div className="sticky top-12 z-10 flex items-center justify-between border-y bg-white px-10 text-xs">
			<button
				onClick={() => setIsFilterOpened((isFilterOpened) => !isFilterOpened)}
				className="flex gap-3 py-4 px-5 hover:bg-slate-50"
			>
				<img src="/icons/filter.svg" alt="filter" />
				<span className="uppercase">Filters</span>
				{isFilterOpened ? (
					<img src="/icons/arrow-up.svg" alt="arrow-up" />
				) : (
					<img src="/icons/arrow-down.svg" alt="arrow-down" />
				)}
			</button>
			<div className="flex gap-4">
				<input
					type="text"
					name="search"
					className="px-10 py-2 text-center text-sm placeholder-[#6b6b6b] focus:outline-none"
					placeholder="Search..."
					onChange={(e) => {
						setFilter(true)
						setSearchWord(e.target.value)
					}}
				/>
			</div>
			<select
				name="sort"
				id="sort"
				onChange={(e) => {
					setFilter(true)
					setSortBy(e.target.value)
				}}
				className="flex gap-3 bg-white py-4 px-5 uppercase hover:cursor-pointer hover:bg-slate-50"
			>
				{sortMethods.map((item, index) => (
					<option key={index} value={item.value}>
						{item.label}
					</option>
				))}
			</select>
		</div>
	)
}
