import { Product } from "@prisma/client"
import { create } from "zustand"
import { persist } from "zustand/middleware"

type WishStore = {
	wishlist: Product[]
	opened: boolean
	toggleWish: () => void
	addToWish: (product: Product) => void
}

export const useWishStore = create<WishStore>()(
	persist(
		(set, get) => ({
			wishlist: [],
			opened: false,
			toggleWish: () => set({ opened: get().opened ? false : true }),

			addToWish: (product: Product) => {
				set((state) => {
					if (state.wishlist) {
						if (state.wishlist.map((p: Product) => p.name).includes(product.name)) {
							return {
								wishlist: [...state.wishlist.filter((p: Product) => p.name !== product.name)],
							}
						}

						return { wishlist: [...state.wishlist, product] }
					}
					return { wishlist: [product] }
				})
			},
		}),
		{
			name: "shomoji-wishlist",
		},
	),
)
