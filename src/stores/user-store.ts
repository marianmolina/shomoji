import { create } from "zustand"
import { persist } from "zustand/middleware"

import { english, french } from "@/locales"

export type Locale = "en" | "fr"
type LocaleStore = {
	locale: Locale
	translation: typeof english
	setLocale: (newLocale: Locale) => void
}

const translations = { en: english, fr: french }

export const useLocaleStore = create<LocaleStore>()(
	persist(
		(set) => ({
			locale: "en",
			translation: translations["en"],
			setLocale: (newLocale: Locale) =>
				set({ locale: newLocale, translation: translations[newLocale] }),
		}),
		{
			name: "shomoji-locale",
		},
	),
)

type AuthStore = {
	auth: boolean
	setAuth: (auth: boolean) => void
}
export const useAuthStore = create<AuthStore>()(
	persist(
		(set) => ({
			auth: false,
			setAuth: (auth: boolean) => {
				return set({ auth: auth })
			},
		}),
		{
			name: "shomoji-auth",
		},
	),
)
