import { Product } from "@prisma/client"
import { create } from "zustand"
import { persist } from "zustand/middleware"

import { CartProduct } from "../types"

type CartStore = {
	cart: CartProduct[]
	opened: boolean
	toggleCart: () => void
	addToCart: (product: Product, selectedColor: string) => void
	updateAmount: (product: CartProduct, method: string) => void
	removeFromCart: (product: CartProduct) => void
	resetCart: () => void
}

export const useCartStore = create<CartStore>()(
	persist(
		(set, get) => ({
			cart: [],

			opened: false,
			toggleCart: () => set({ opened: get().opened ? false : true }),

			addToCart: (product: Product, selectedColor: string) => {
				const cartProduct = {
					name: product.name,
					color: selectedColor || product.variants[0].color,
					price: product.price,
					amount: 1,
					totalPrice: product.price,
					image: selectedColor
						? product.variants.filter((variant) => variant.color === selectedColor)[0].image
						: product.variants[0].image,
				}

				set((state) => {
					if (get().cart) {
						if (
							state.cart.map((p: CartProduct) => p.name).includes(cartProduct.name) &&
							state.cart.map((p: CartProduct) => p.color).includes(cartProduct.color)
						) {
							const itemToIncrement = state.cart.filter(
								(p: CartProduct) => p.name === cartProduct.name,
							)[0]
							const restCart = state.cart.filter((p: CartProduct) => p.name !== product.name)
							const updatedProduct = {
								...cartProduct,
								amount: itemToIncrement.amount + 1,
								totalPrice: product.price * (itemToIncrement.amount + 1),
							}
							return { cart: [...restCart, updatedProduct] }
						}
						return { cart: [...state.cart, cartProduct] }
					}
					return { cart: [cartProduct] }
				})
			},

			updateAmount: (product: CartProduct, method: string) => {
				set((state) => {
					const currentProduct = state.cart.filter((p: CartProduct) => p.name === product.name)[0]
					const currentProductIndex = state.cart.indexOf(currentProduct)
					const updatedProduct = {
						...currentProduct,
						amount: method === "decrement" ? currentProduct.amount - 1 : currentProduct.amount + 1,
						totalPrice:
							currentProduct.price *
							(method === "decrement" ? currentProduct.amount - 1 : currentProduct.amount + 1),
					}
					const begCart = state.cart.slice(0, currentProductIndex)
					const endCart = state.cart.slice(currentProductIndex + 1)

					return { cart: [...begCart, updatedProduct, ...endCart] }
				})
			},

			removeFromCart: (product: CartProduct) => {
				set((state) => ({
					cart: [
						...state.cart.filter((p: CartProduct) =>
							p.name === product.name ? p.color !== product.color : p.name !== product.name,
						),
					],
				}))
			},

			resetCart: () => set({ cart: [] }),
		}),
		{
			name: "shomoji-cart",
		},
	),
)
