import "./main.css"

import { QueryClient, QueryClientProvider } from "@tanstack/react-query"
import { httpBatchLink } from "@trpc/client"
import { StrictMode } from "react"
import { createRoot } from "react-dom/client"
import { createBrowserRouter, RouterProvider } from "react-router-dom"

import { ErrorPage } from "@/layouts/error-page"
import { Account, Checkout, Login, Register, Root, Shop } from "@/routes"
import { env, trpc } from "@/utils"

const queryClient = new QueryClient({
	defaultOptions: {
		queries: { refetchOnWindowFocus: false, retry: 0, staleTime: 1000 * 60 * 10 },
		mutations: { retry: 0 },
	},
})
const trpcClient = trpc.createClient({
	links: [
		httpBatchLink({
			url: env.api.url,
			fetch: (url, options) => fetch(url, { ...options, credentials: "include" }),
		}),
	],
})

const router = createBrowserRouter([
	{
		path: "/",
		element: <Root />,
		errorElement: <ErrorPage />,
		children: [
			{
				errorElement: <ErrorPage />,
				children: [
					{
						index: true,
						element: <Shop />,
					},
					{
						path: "checkout",
						element: <Checkout />,
					},
					{
						path: "login",
						element: <Login />,
					},
					{
						path: "register",
						element: <Register />,
					},
					{
						path: "account",
						element: <Account />,
					},
				],
			},
		],
	},
])

createRoot(document.getElementById("root") as HTMLElement).render(
	<StrictMode>
		<trpc.Provider client={trpcClient} queryClient={queryClient}>
			<QueryClientProvider client={queryClient}>
				<RouterProvider router={router} />
			</QueryClientProvider>
		</trpc.Provider>
	</StrictMode>,
)
