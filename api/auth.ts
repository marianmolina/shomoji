import { User } from "@prisma/client"
import type { Request } from "express"
import { sign, verify } from "jsonwebtoken"

export const generateToken = (data: object) => sign(data, process.env.JWT_SECRET as string)
export const decodeToken = <T>(token: string) =>
	verify(token, process.env.JWT_SECRET as string) as T

const parseCookies = (cookies: string) => {
	return cookies.split("; ").reduce((cookies, cookie) => {
		const [name, value] = cookie.split("=")
		return { ...cookies, [name]: value }
	}, {} as { token?: string })
}

export const getUserFromRequest = (req: Request) => {
	const token = parseCookies(req.headers.cookie || "")?.token
	if (!token) return null

	try {
		return decodeToken<Omit<User, "name">>(token)
	} catch {
		return null
	}
}
