import { hash, verify } from "argon2"
import { z } from "zod"

import { generateToken } from "../auth"
import { trpc } from "../trpc"

const oneMonthInMs = 1000 * 60 * 60 * 24 * 30

export const users = trpc.router({
	register: trpc.procedure
		.input(z.object({ email: z.string().email(), password: z.string() }))
		.mutation(async ({ input, ctx }) => {
			const exists = await ctx.prisma.user.findUnique({
				where: { email: input.email },
			})
			if (exists) throw new Error("User already exists")

			const password = await hash(input.password)
			const user = await ctx.prisma.user.create({
				data: { email: input.email, cart: [], wishlist: [], purchases: [] },
			})

			await ctx.prisma.account.create({ data: { password, userId: user.id } })

			const token = generateToken({ id: user.id, email: user.email })
			ctx.res.cookie("token", token, {
				maxAge: oneMonthInMs,
				httpOnly: true,
				secure: true,
				sameSite: "lax",
			})

			return true
		}),

	login: trpc.procedure
		.input(z.object({ email: z.string().email(), password: z.string() }))
		.mutation(async ({ input, ctx }) => {
			const { email, password } = input

			const account = await ctx.prisma.account.findFirst({
				where: { user: { email } },
				include: { user: true },
			})
			if (!account) throw new Error("Invalid credentials")

			const valid = await verify(account.password, password)
			if (!valid) throw new Error("Invalid credentials")

			const token = generateToken({ id: account.user.id, email: account.user.email })
			ctx.res.cookie("token", token, {
				maxAge: oneMonthInMs,
				httpOnly: true,
				secure: true,
				sameSite: "lax",
			})
			return true
		}),

	logout: trpc.procedure.mutation(({ ctx }) => {
		ctx.res.clearCookie("token", { httpOnly: true, secure: true, sameSite: "lax" })
		return true
	}),

	current: trpc.procedure.query(({ ctx }) => {
		if (!ctx.user) return null
		return ctx.prisma.user.findUnique({ where: { email: ctx.user.email } })
	}),
})
