import { z } from "zod"

import { trpc } from "../trpc"

const Variant = z.object({
	color: z.string(),
	image: z.string(),
})
const Product = z.object({
	id: z.string(),
	slug: z.string(),
	name: z.string(),
	price: z.number(),
	categories: z.array(z.string()),
	variants: z.array(Variant),
	ratings: z.array(z.number()),
	instock: z.number(),
})

export const products = trpc.router({
	getProducts: trpc.procedure.query(({ ctx }) => {
		const products = ctx.prisma.product.findMany()
		return products
	}),
	updateRating: trpc.procedure
		.input(z.object({ product: Product, rating: z.number() }))
		.mutation(async ({ input, ctx }) => {
			const oldRatings = await ctx.prisma.product
				.findUnique({
					where: { slug: input.product.slug },
				})
				.then((value) => value?.ratings)
			await ctx.prisma.product.update({
				where: { slug: input.product.slug },
				data: {
					ratings: oldRatings ? [...oldRatings, input.rating] : [input.rating],
				},
			})
		}),
})
