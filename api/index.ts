import { createExpressMiddleware } from "@trpc/server/adapters/express"
import * as cors from "cors"
import * as express from "express"

import { products, users } from "./routers"
import { createContext, trpc } from "./trpc"

const app = express()

const router = trpc.router({
	products,
	users,
})

app.use(cors({ origin: process.env.APP_URL, credentials: true }))
app.use(
	"/api",
	createExpressMiddleware({
		router,
		createContext,
	}),
)
const port = process.env.PORT || 5000
app.listen(port, () => console.log("Server is live"))

export type Router = typeof router
