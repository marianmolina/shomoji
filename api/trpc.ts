import { PrismaClient } from "@prisma/client"
import { inferAsyncReturnType, initTRPC } from "@trpc/server"
import { CreateExpressContextOptions } from "@trpc/server/adapters/express"

import { getUserFromRequest } from "./auth"

const prisma = new PrismaClient()

export const createContext = async ({ req, res }: CreateExpressContextOptions) => {
	const user = getUserFromRequest(req)
	return { req, res, prisma, user }
}

type Context = inferAsyncReturnType<typeof createContext>
export const trpc = initTRPC.context<Context>().create()
