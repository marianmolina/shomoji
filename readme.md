# Shomoji

A full stack type-safe and light-weight ecommerce prototype using Typescript, React, Zustand (global client state management), tRPC (type-safe API calls with React Query bindings on the client) and Prisma (ORM for Typescript and Node.js).

<br/>

## Features

### Shop

- List of products that can be sorted, filtered by category (menu), keyword (search), color, price and availability (filters).
- Pagination
- Products can be added to a cart with a selected color
- Products can be added to a wishlist
- Products can be rated from 1 to 5 stars

### Cart

- View cart products by their selected color, update their amount (increase and decrease), remove from the cart
- View total price
- Proceed to checkout page if user connected

### Others

- Pick between english and french language (not finished)
- Cart and wishlist data persisted in localstorage
- New ratings persisted in DB
- Register new user account, login and logout
- User account persister in DB

<br/>
<br/>

## Coming later

### UI/UX

- Responsiveness (the current version was built from a laptop with a screen resolution of 1366 x 768 pixels)
- Add some subtle animations
- Implement suspens fallback for the product list
- Add user feedback on certain actions (need to be logged in to access checkout, purchased validated, etc.)

### User data

- Persist user cart and wishlist in the DB
- Keep a record of the user purchase history

### User account

- Enable user to modify account password and delete account
- Integrate account validation by email
- Enforce stricter password
- Add option to view/hide password
- Add password verification on register and modify

### Others

- Integrate online payment solution
- Update amount of product instock in the DB after user purchase
- Improve rating system: only user who purchased product can rate it, only once
- Implement language translation accross the application
- Implement currency option
- Implement sort product by rating
- Improve filtering implementation (probably handle it with Zustand)
- Implement protected routes

<br/>
<br/>

## Ressources I used to build the application

### Tooling

[Configuring ESLint and Prettier for TypeScript](https://www.youtube.com/watch?v=11jpa8e5jEQ)  
[How to use Prettier with ESLint](https://www.robinwieruch.de/prettier-eslint/)

### Backend

[Prisma with MongoDB and Typescript](https://www.prisma.io/docs/getting-started/setup-prisma/start-from-scratch/mongodb-typescript-mongodb)  
[TRPC with Express](https://trpc.io/docs/express)  
[Let's Learn tRPC! - Learn With Jason](https://www.youtube.com/watch?v=GryES84SSEU) (not up to date but super interesting)

### Frontend

[Zustand tutorial](https://dev.to/franklin030601/using-zustand-with-react-js-9di)  
[Zustand documentation](https://github.com/pmndrs/zustand#readme)  
[TRPC with React](https://trpc.io/docs/react)  
[React Router 6.4 - Learn With Jason](https://www.youtube.com/watch?v=kGtHwLzmMBQ&t=2515s)  
[All About React Query (with Tanner Linsley) — Learn With Jason](https://www.youtube.com/watch?v=DocXo3gqGdI) (not up to date but super interesting)

### Illustrations

[Emojis](https://www.figma.com/community/file/1182976024086113944) (I modified certain in Figma to have different colors)  
[Countryside image](https://pixabay.com/users/aalmeidah-4277022/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=4741364)
